
rails g scaffold Suppliers name:string supplier_type:string
rails db:migrate
rails g bootstrap:themed Suppliers
a

rails g scaffold OenologicalProducts name:string supplier_references
rails db:migrate
rails g bootstrap:themed OenologicalProducts
a

rails g scaffold WineTypes name:string
rails db:migrate
rails g bootstrap:themed WineTypes
a

rails g scaffold Wines name:string wine_type:references supplier:references degree:float
rails db:migrate
rails g bootstrap:themed Wines
a

rails g scaffold TankTypes name:string
rails db:migrate
rails g bootstrap:themed TankTypes
a

rails g scaffold Tanks name:string tank_type:references capacity:float
rails db:migrate
rails g bootstrap:themed Tanks
a

rails g scaffold WineTanks name:string wine:references tank:references
rails db:migrate
rails g bootstrap:themed WineTanks
a

rails g scaffold TankOenologicalProducts oenological_product:references tank:references weight:float quantity:integer
rails db:migrate
rails g bootstrap:themed TankOenologicalProducts
a

rails g scaffold Assemblies name:string type:string reference:string tank:references
rails db:migrate
rails g bootstrap:themed Assemblies
a

rails g scaffold PackageTypes name:string capacity:integer
rails db:migrate
rails g bootstrap:themed PackageTypes
a

rails g scaffold Packages package_type:references assembly:references
rails db:migrate
rails g bootstrap:themed Packages
a

rails g scaffold Clients name:string client_type:string
rails db:migrate
rails g bootstrap:themed Clients
a

rails g scaffold Orders client:references package:references order_type:string
rails db:migrate
rails g bootstrap:themed Orders
a

rails g scaffold OrderPackages order:references package:references quantity:integer
rails db:migrate
rails g bootstrap:themed OrderPackages
a

