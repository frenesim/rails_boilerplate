Rails.application.routes.draw do

  root to: 'dashboard#index'
  resources :order_packages
  resources :orders
  resources :clients
  resources :packages
  resources :assemblies
  resources :package_types
  resources :tank_oenological_products
  resources :wine_tanks
  resources :tanks
  resources :tank_types
  resources :wines
  resources :wine_types
  resources :oenological_products
  resources :suppliers
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
