class CreatePackages < ActiveRecord::Migration[5.0]
  def change
    create_table :packages do |t|
      t.references :package_type, foreign_key: true
      t.references :assembly, foreign_key: true

      t.timestamps
    end
  end
end
