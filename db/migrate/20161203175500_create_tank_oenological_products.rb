class CreateTankOenologicalProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :tank_oenological_products do |t|
      t.references :oenological_product, foreign_key: true
      t.references :tank, foreign_key: true
      t.float :weight
      t.integer :quantity

      t.timestamps
    end
  end
end
