class CreateTanks < ActiveRecord::Migration[5.0]
  def change
    create_table :tanks do |t|
      t.string :name
      t.references :tank_type, foreign_key: true
      t.float :capacity

      t.timestamps
    end
  end
end
