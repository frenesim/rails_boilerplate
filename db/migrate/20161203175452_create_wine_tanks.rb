class CreateWineTanks < ActiveRecord::Migration[5.0]
  def change
    create_table :wine_tanks do |t|
      t.string :name
      t.references :wine, foreign_key: true
      t.references :tank, foreign_key: true

      t.timestamps
    end
  end
end
