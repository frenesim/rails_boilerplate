class CreateWines < ActiveRecord::Migration[5.0]
  def change
    create_table :wines do |t|
      t.string :name
      t.references :wine_type, foreign_key: true
      t.references :supplier, foreign_key: true
      t.float :degree

      t.timestamps
    end
  end
end
