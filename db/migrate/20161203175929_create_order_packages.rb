class CreateOrderPackages < ActiveRecord::Migration[5.0]
  def change
    create_table :order_packages do |t|
      t.references :order, foreign_key: true
      t.references :package, foreign_key: true
      t.integer :quantity

      t.timestamps
    end
  end
end
