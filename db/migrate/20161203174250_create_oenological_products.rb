class CreateOenologicalProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :oenological_products do |t|
      t.string :name
      t.string :supplier_references

      t.timestamps
    end
  end
end
