class CreateAssemblies < ActiveRecord::Migration[5.0]
  def change
    create_table :assemblies do |t|
      t.string :name
      t.string :type
      t.string :reference
      t.references :tank, foreign_key: true

      t.timestamps
    end
  end
end
