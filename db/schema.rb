# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161203175929) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "assemblies", force: :cascade do |t|
    t.string   "name"
    t.string   "type"
    t.string   "reference"
    t.integer  "tank_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tank_id"], name: "index_assemblies_on_tank_id", using: :btree
  end

  create_table "clients", force: :cascade do |t|
    t.string   "name"
    t.string   "client_type"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "oenological_products", force: :cascade do |t|
    t.string   "name"
    t.string   "supplier_references"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "order_packages", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "package_id"
    t.integer  "quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_id"], name: "index_order_packages_on_order_id", using: :btree
    t.index ["package_id"], name: "index_order_packages_on_package_id", using: :btree
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "client_id"
    t.integer  "package_id"
    t.string   "order_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["client_id"], name: "index_orders_on_client_id", using: :btree
    t.index ["package_id"], name: "index_orders_on_package_id", using: :btree
  end

  create_table "package_types", force: :cascade do |t|
    t.string   "name"
    t.integer  "capacity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "packages", force: :cascade do |t|
    t.integer  "package_type_id"
    t.integer  "assembly_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["assembly_id"], name: "index_packages_on_assembly_id", using: :btree
    t.index ["package_type_id"], name: "index_packages_on_package_type_id", using: :btree
  end

  create_table "suppliers", force: :cascade do |t|
    t.string   "name"
    t.string   "supplier_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "tank_oenological_products", force: :cascade do |t|
    t.integer  "oenological_product_id"
    t.integer  "tank_id"
    t.float    "weight"
    t.integer  "quantity"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["oenological_product_id"], name: "index_tank_oenological_products_on_oenological_product_id", using: :btree
    t.index ["tank_id"], name: "index_tank_oenological_products_on_tank_id", using: :btree
  end

  create_table "tank_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tanks", force: :cascade do |t|
    t.string   "name"
    t.integer  "tank_type_id"
    t.float    "capacity"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["tank_type_id"], name: "index_tanks_on_tank_type_id", using: :btree
  end

  create_table "wine_tanks", force: :cascade do |t|
    t.string   "name"
    t.integer  "wine_id"
    t.integer  "tank_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tank_id"], name: "index_wine_tanks_on_tank_id", using: :btree
    t.index ["wine_id"], name: "index_wine_tanks_on_wine_id", using: :btree
  end

  create_table "wine_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "wines", force: :cascade do |t|
    t.string   "name"
    t.integer  "wine_type_id"
    t.integer  "supplier_id"
    t.float    "degree"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["supplier_id"], name: "index_wines_on_supplier_id", using: :btree
    t.index ["wine_type_id"], name: "index_wines_on_wine_type_id", using: :btree
  end

  add_foreign_key "assemblies", "tanks"
  add_foreign_key "order_packages", "orders"
  add_foreign_key "order_packages", "packages"
  add_foreign_key "orders", "clients"
  add_foreign_key "orders", "packages"
  add_foreign_key "packages", "assemblies"
  add_foreign_key "packages", "package_types"
  add_foreign_key "tank_oenological_products", "oenological_products"
  add_foreign_key "tank_oenological_products", "tanks"
  add_foreign_key "tanks", "tank_types"
  add_foreign_key "wine_tanks", "tanks"
  add_foreign_key "wine_tanks", "wines"
  add_foreign_key "wines", "suppliers"
  add_foreign_key "wines", "wine_types"
end
